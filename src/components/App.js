import '../styles/App.css';
import Chat from './Chat/Chat';
import Sidebar from './Sidebar';
import React, { useState, useEffect } from 'react';

function App() {
  return (
    <div className="App">
      <Chat />
      <Sidebar />
    </div>
  );
}

export default App;
