import '../styles/Sidebar.css';
import logoNoBackground from '../assets/images/logo-no-background.png'
import React, { useState, useEffect } from 'react';

function Sidebar() {
  return (
    <div className="Sidebar">
      <img id="sidebarTopLogo" src={logoNoBackground} />
    </div>
  );
}

export default Sidebar;
