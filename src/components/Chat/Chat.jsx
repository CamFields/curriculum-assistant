import Header from './Header';
import PromptBox from './PromptBox';
import '../../styles/Chat/Chat.css';
import React, { useState, useEffect } from 'react';
import aiIcon from '../../assets/images/logo-only.png'

function Chat() {
  let [messages, setMessages] = useState([]);

  return (
    <div className="Chat">
      <Header />
      <PromptBox messages={messages} setMessages={setMessages}/>
      <div className="messagesContainer">
        {messages.map((message, index) => (
          <div key={index} className={`message ${message.type}`}>
            {`${message.type === 'ai' ? '🤖' : '👩‍🏫'}: ${message.text}`}
          </div>
        ))}
      </div>
    </div>
  );
}

export default Chat;
