import '../../styles/Chat/Header.css';
import downArr from '../../assets/images/down-arrow-image.png'
import downloadBtn from '../../assets/images/Download-Button.png'

function Header() {
  return (
    <div className="Header">
      <button id="model-button">ModelName{'\u00A0'}<img id="downArr" src={downArr}></img></button>
      <button id="downloadBtn"><img id="downloadBtnIcon" src={downloadBtn} /></button>
    </div>
  );
}

export default Header;
