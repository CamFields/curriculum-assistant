import '../../styles/Chat/PromptBox.css';
import submitArr from '../../assets/images/submitArr.png'
import React, { useState, useEffect } from 'react';
import axios from 'axios';


function PromptBox({messages, setMessages}) {
  const [inputValue, setInputValue] = useState('');
  // let [messages, setMessages] = useState([]);

  const handleChange = (event) => {
    setInputValue(event.target.value);
    console.log(messages);
  };

  const handleSubmit = async (event) => {
    event.preventDefault();

    // Add current input value to messages
    setMessages(prevMessages => [...prevMessages, { type: 'user', text: inputValue }]);

    try {
      // Sending request to the server
      const response = await axios.post('http://localhost:3000/api/openai', { prompt: inputValue });
      console.log(`RESPONSE: ${response}`);
      // Add OpenAI response to messages
      setMessages(prevMessages => [...prevMessages, { type: 'ai', text: response.data.choices[0].text }]);
    } catch (error) {
      console.error('Error in fetching response from OpenAI:', error);
      // Handle error (e.g., display an error message)
    }

    // Clear the input field
    console.log('made it')
    setInputValue('');
  };


  return (
    <div className="PromptBox">
    <form onSubmit={handleSubmit} id="promptForm">
      <input type="text" id="promptInput" name="chatPromptInput" placeholder="Message Curriculum Assistant..." onChange={handleChange} value={inputValue}></input>
    </form>
      <a id="belowPromptMessage" href="https://platform.openai.com/docs/guides/prompt-engineering?utm_source=tldrnewsletter">Having trouble getting the response your looking for? Consider <b>Prompt Engineering</b></a>
    </div>
  );
}

export default PromptBox;
