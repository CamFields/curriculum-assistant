require('dotenv').config();
const express = require('express');
const bodyParser = require('body-parser');
const axios = require('axios');
const cors = require('cors');

const app = express();
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

const port = process.env.PORT || 3001;
// const port = 3001;

const assistantId = process.env.ASSISTANT_ID;
const openAiApiKey = process.env.OPENAI_API_KEY;

// Middleware to parse JSON bodies
app.use(bodyParser.json());

// Route to handle requests to the OpenAI API
app.post('/api/openai', async (req, res) => {
  try {
    const response = await axios.post('https://api.openai.com/v1/completions', {
      model: "text-davinci-003", // Replace with your custom model ID
      prompt: req.body.prompt,
      max_tokens: 150
      // Add other parameters here as needed
    }, {
      headers: {
        'Authorization': `Bearer ${openAiApiKey}`,
        'Content-Type': 'application/json',
        // 'OpenAI-Beta': 'assistants=v1'
      }
    });

    res.json(response.data);
  } catch (error) {
    res.status(500).json({ error: error.message });
    console.log(res)
  }
});



// Start the server
app.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});
